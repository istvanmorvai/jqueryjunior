function showHeight( element, height ) {
  $( "div" ).text( "The height for the " + element + " is " + height + "px." );
}
$( "#getp" ).click(function() {
  showHeight( "paragraph", $( "p" ).height() );
});
$( "#getd" ).click(function() {
  showHeight( "document", $( document ).height() );
});
$( "#getw" ).click(function() {
  showHeight( "window", $( window ).height() );
});