var count = 0;
$( "p" ).each(function() {
  var $thisParagraph = $( this );
  var count = 0;
  $thisParagraph.click(function() {
    count++;
    $thisParagraph.find( "span" ).text( "clicks: " + count );
    $thisParagraph.toggleClass( "highlight", count % 3 === 0 );
  });
});